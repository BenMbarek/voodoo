# what was convenient 

- the sdk is easy to integrate and takes a few minutes to test that start correctly

# points to improve:

- Developer is obliged to download the aar files and add it to the libs folder instead of being automatically retrieved from jcenter or maven repo.
- Developer is also obliged to add all the dependencies that the sdk needs in his project.These dependencies can be configured in the sdk
- The dependencies used by the sdk are very old (example: 8 major versions of delay for play-services-ads, not official version of volley libs)
- Some annotations are missing for better support of integration in kotlin project (example: @NonNull for Map in video player callback)
- The documentation is not complete (example: the documentation does not indicate that the callbacks like "vidCoinDidValidateView" used enum and that it is necessary to convert it to strings in order use it)
- we can also improve the reactivity of sdk by using some libs like RxJava or liveData to better follow the life cycle of the page start the sdk and avoid some leaks