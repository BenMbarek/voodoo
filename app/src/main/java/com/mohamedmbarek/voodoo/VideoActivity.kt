package com.mohamedmbarek.voodoo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

import com.vidcoin.sdkandroid.VidCoin
import com.vidcoin.sdkandroid.core.VidCoinBase
import com.vidcoin.sdkandroid.core.VidCoinBase.VCStatusCode.*
import com.vidcoin.sdkandroid.core.interfaces.VidCoinCallBack
import kotlinx.android.synthetic.main.activity_video.*
import java.util.*


class VideoActivity : AppCompatActivity(), VidCoinCallBack {

    private val vdCoin: VidCoin by lazy { VidCoin.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        //init player
        vdCoin.init(this, APP_ID, this)
        // Enable the log in the console
        vdCoin.setVerboseTag(true)
        btnWatchVideo.setOnClickListener { playAd() }
    }

    private fun playAd() {
        vdCoin.playAdForPlacement(this, PLACEMENT_CODE, -1)
    }


    override fun onStart() {
        super.onStart()
        vdCoin.onStart()
    }

    override fun onStop() {
        super.onStop()
        vdCoin.onStop()
    }

    /**
     *  Each time an event appears on a campaign this callback is calls
     */
    override fun vidCoinCampaignsUpdate() {
        // checking if a video is available
        if (vdCoin.videoIsAvailableForPlacement(PLACEMENT_CODE)) runOnUiThread {
            tvNoVideoAvailable.visibility = View.GONE
            btnWatchVideo.visibility = View.VISIBLE
        } else runOnUiThread {
            btnWatchVideo.visibility = View.GONE
            tvNoVideoAvailable.visibility = View.VISIBLE
        }
    }

    /**
     * This method will be call when the request has been validated on the server
     */
    override fun vidCoinDidValidateView(vcData: HashMap<VidCoinBase.VCData, String>) {
        vcData[VidCoinBase.VCData.VC_DATA_STATUS_CODE]?.let {
            when (valueOf(it)) {
                VC_STATUS_CODE_SUCCESS -> FeatureActivity.startActivity(this)
                VC_STATUS_CODE_CANCEL -> TODO()
                VC_STATUS_CODE_ERROR -> TODO()
            }
        }
    }

    /**
     * After our view has been removed from screen, this method is call with some information
     */
    override fun vidCoinViewDidDisappearWithViewInformation(vcData: HashMap<VidCoinBase.VCData, String>) {

    }

    /**
     * Before our view will be display on screen, this method is calls
     */
    override fun vidCoinViewWillAppear() {
        // Here you can switch off your app's sound
    }


    companion object {
        private const val APP_ID = "908055757079910"
        private const val PLACEMENT_CODE = "i7ozkhzfil4ccc4okw8w0c8gwk0c4wg"
    }
}