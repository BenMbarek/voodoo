package com.mohamedmbarek.voodoo

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class FeatureActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feature)
    }

    companion object {

        fun startActivity(context: Context){
            context.startActivity(Intent(context,FeatureActivity::class.java ))
        }
    }
}
